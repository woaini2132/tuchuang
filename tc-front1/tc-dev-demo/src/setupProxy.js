const { createProxyMiddleware } = require('http-proxy-middleware');


module.exports= function(app) {
    console.log('http-proxy-middleware启用...')
    app.use(
        createProxyMiddleware(
            '/api',
            {
                //target: 'http://42.194.128.13',
                 target: 'http://192.168.28.128',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/'
                }
            }
        )
    )
}
