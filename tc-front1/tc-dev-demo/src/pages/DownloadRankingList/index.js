import React, {useEffect, useState} from 'react'
import { Table, Button, message } from 'antd'
import { ReloadOutlined } from '@ant-design/icons';
import { getShareFileDownloadRankingList } from '../../apis/user'
export default function DownloadRankingList(props) {
    const columns = [{
        title: '排名',
        width: 50,
        align: 'center',
        render: (text, item, index) => <div style={{ align: 'center' }}>{++index}</div>
    },{
        title: '文件名',
        width: 100,
        align: 'center',
        dataIndex: 'filename',
    },{
        title: '下载量',
        width: 100,
        align: 'center',
        dataIndex: 'pv'
    }]
    
    // 点击刷新按钮时(做一下防抖)
    const onRefreshBtnClick = () => {
        if (refreshTimer) {
            console.log('刷新按钮冷却中...')
            return 
        }
        refreshTable();
        //嵌套setTimeout定时器，在1秒后执行回调函数，将refreshTimer置为null
        setRefreshTimer(setTimeout(() => {
            setRefreshTimer(null)
        }, 1000))
    }

    // 刷新
    const refreshTable = () => {
        getShareFileDownloadRankingList({count: pageConfig.page_size, start:0})
        .then(data => {
            const {code, files = []} = data
            // console.log('获取的排行榜响应数据', data)
            setDataList(files)
            
        }).catch(error => {
            console.log('获取排行榜响应数据失败', error)
        })
    }

    // 页面更改时
    const changePage = (page) => {
        //换页时的开始索引，第一页为0，第二页为10.。。。
        const start = (page-1) * pageConfig.page_size
        //更新pageConfig中的page_no为当前页码
        //setpage用展开运算符取出pageConfig中的所有属性，然后覆盖page_no属性
        setPage({ ...pageConfig, page_no: page})
        getShareFileDownloadRankingList({count: pageConfig.page_size, start: start})
        .then(data => {
            const {code, total, files = []} = data
            // console.log('获取的排行榜响应数据', data)
            setDataList(files)
            
        }).catch(error => {
            console.log('获取排行榜响应数据失败', error)
        })
        
    }

    const [total, setTotal] = useState(0)//一共多少文件
    const [dataList, setDataList] = useState([])//文件列表
    const [refreshTimer, setRefreshTimer] = useState(null)//是否在刷新冷却中

    const [pageConfig, setPage] = useState({
        total_page: 0, // 总页数
        page_no: 1, // 当前页码
        page_size: 10 // 每页数据条数
    });
    
    // 首次刷新，空数组，只刷新一次
    useEffect(() => {
        getShareFileDownloadRankingList({count:pageConfig.page_size, start:0})
        .then(data => {
            const {code, total, files = []} = data
            console.log('获取的排行榜响应数据', data)
            setDataList(files)
            setTotal(total)
        }).catch(error => {
            message.error('获取排行榜数据失败')
        })
    }, [])

    // 分页设置
    const pagination = {
        //分页在右下角显示
        position: ['bottomRight'],
        size: 'small',
        //默认页码为1
        defaultCurrent: 1,
        total: total,
        pageSize: pageConfig.page_size,
        current: pageConfig.page_no,
        showTotal: total => `共${total}条`,
        //页码改变时的回调，参数是改变后的页码及每页条数
        onChange: changePage
    }

    return (
        <div>
            <div style={{marginLeft: 10 + 'px', display: 'flex'}}>
                <Button 
                    icon={<ReloadOutlined />}
                    onClick={onRefreshBtnClick}
                >刷新</Button>
            </div>
            <div className="container">
                <Table columns={columns}  
                    dataSource={dataList}
                    total={total}  
                    //以索引作为key
                    rowKey={(item, index) => index}
                    pagination={pagination} 
                />
            </div>
        </div>
        
    )
}