import React, { useState, useEffect } from 'react';
import { useLocation, Redirect } from 'react-router-dom';
import { setCookie } from '../../utils/utils'

export default function Logout(props) {
    //空数组，只会渲染一次
    const [loggedIn, setLoggedIn] = useState(true);
    useEffect(() => {
        logout().then(() => {
            setLoggedIn(false);
        });
    }, []);

    const logout = () => {
        //固定写法，创建Promise对象。
        //resolve()表示成功，不打印任何信息。reject()表示失败
        return new Promise((resolve, reject) => {
            // 将设置的cookie值覆盖为空字符串
            setCookie({userName: '', token: ''})
            resolve()
        })
    }
    // 当为false，表示没有登录，跳转到登录页面
    if (!loggedIn) {
        return <Redirect to={{ pathname: '/login' }} />
    }
    
    return (
        <div style={{ display: "flex", alignItems: "center", justifyContent: "center", marginTop: 100 }}>
            正在退出登录...
        </div>
    );
}