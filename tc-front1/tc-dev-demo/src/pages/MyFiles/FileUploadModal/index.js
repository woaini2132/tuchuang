import React, { useState } from 'react';
import { Modal, Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import axios from 'axios'
import SparkMD5 from 'spark-md5'
import { getUserFilesCount, getUserFilesInfo} from '../../../apis/user';
import { checkMD5OfFile } from '../../../apis/user'
export default function FileUploadModal(props) {
    const { visible, onOk, token, user, footer } = props
    const [fileList, setFileList] = useState([])
    // 上传组件需要的属性
    const uploadProps = {
        className: 'upload-list-inline',
        //最大上传数 
        maxCount: 1,
        // 上传文件被改变时
        onChange: ({file, fileList}) => {
            setFileList(fileList)
        },
        
    }
    // 上传文件之前的逻辑
    const beforeUpload = (file) => {
        return new Promise((resolve, reject) => {
            const isLt10M = file.size / 1024 / 1024 < 10
            if (!isLt10M) {
                message.error('文件大小不能大于10M')
                reject('文件大小不能大于10M')
            }
            getMD5OfFile(file).then(md5 => {
                //console.log('文件的md5值为:', md5)
                //console.log('参数', token, user,file.name, md5)
                checkMD5OfFile({ token, user, filename: file.name, md5})
                .then(data => {
                    //console.log('md5秒传的返回结果为:', data)
                    //返回的res.data只包含code字段
                    const { code } = data
                    switch (code) {
                        // 秒传成功
                        case 0:
                            file.md5 = ''
                            message.error('秒传成功,取消文件上传')
                            reject()
                            break;
                        // 秒传失败时需要上传文件
                        case 1:
                            file.md5 = md5
                            // console.log('设置需发送给后端的md5值为:',md5)
                            message.success({content: '等待文件上传', key: 'upload' })
                            resolve()
                            break;
                        // token校验失败
                        case 4:
                            file.md5 = ''
                            message.error('token校验失败,取消文件上传')
                            reject()
                            break;
                        // 5.文件已存在
                        default:
                            file.md5 = ''
                            message.error('文件已存在,取消文件上传')
                            reject()
                            break;
                    }
                })  
            })
        
        })
                            
    }
    // 自定义上传逻辑
    const customRequest = (options) => {
        console.log('自定义上传', options)
        const { onSuccess, onError, file, onProgress } = options
        // 上传地址
        const action = 'api/upload'
        // 需要添加到formData中的参数
        const uploadParams = {
            user: user,
            md5: file.md5,
            size: file.size
        }

        let formData = new FormData();
        
        formData.append('file', file);

        for(let param in uploadParams) {
            formData.append(param, uploadParams[param])
        }
        console.log('打印自定义上传时存储的md5:', uploadParams.md5)
        
        // 发送上传文件请求
        axios.post(action, formData)
        .then(res=> {
            const { code } = res.data;
            // 打印发送的请求数据
            if (code === 0) {
                message.success({content: '文件上传成功', key: 'upload'})
                onSuccess()
                // refreshTable()
            }else{
                onError()
            }
        })
        .catch(error => {
            message.error('图片上传失败')
            onError()
        })
    }

    // 获取文件内容的md5值
    const getMD5OfFile = (file) => {
        return new Promise(function(resolve, reject){
            //File.prototype.slice作用是将文件切片
            let blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
            chunkSize = 2097152,   // 一次读取2MB
            //Math.ceil()向上取整，得到需要多少块
            chunks = Math.ceil(file.size / chunkSize),
            //当前处理的块索引
            currentChunk = 0,
            //创建md5对象（基于SparkMD5）
            spark = new SparkMD5.ArrayBuffer(),
            //FileReader对象允许Web应用程序异步读取存储在用户计算机上的文件
            fileReader = new FileReader();

            const loadNext = () => {
                //起始字节位置
                let start = currentChunk * chunkSize
                //结束字节位置
                let end = start + chunkSize >= file.size ? file.size : start + chunkSize
                //固定写法，表示读取文件的一部分内容
                //console.log('blobSlice.', blobSlice)
                fileReader.readAsArrayBuffer(blobSlice.call(file, start, end))
            }

            fileReader.onload = function(e) {
                //console.log('e',e)
                console.log(`read chunk ${currentChunk + 1} of ${chunks}`)
                console.log('spark.app',e.target.result)
                spark.append(e.target.result)
                console.log('spark',spark.end())
                currentChunk ++
    
                if (currentChunk < chunks) {
                    loadNext()
                }else{
                    console.log('文件读取完成')
                    const md5 = spark.end()
                    console.log('产生的md5为:', md5)
                    resolve(md5)
                }
            }
            fileReader.onerror = function(e) {
                console.log('文件读取出错')
                reject('')
            }  
            loadNext()
        })
    }

    return (
        <Modal width={600} visible={visible} closable={false} okText="关闭" onOk={onOk} footer={footer}  >
            <div key={Math.random}>
                <Upload 
                    {...uploadProps} 
                    customRequest={customRequest}
                    beforeUpload={beforeUpload}
                    fileList={fileList}
                    showUploadList={true}
                >
                    <Button
                        type="primary"
                        icon={<UploadOutlined />}
                    >
                        上传文件
                    </Button>
                </Upload>
            </div> 
        </Modal>
    )
}