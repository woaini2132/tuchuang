import React, {useEffect, useState} from 'react'
import { Modal, Button, Input, Tooltip, Form, message, Card, Descriptions, Image } from 'antd'
import { CopyOutlined } from '@ant-design/icons';
import { useLocation, Redirect  } from 'react-router-dom'
import { browseShareImage } from '../../apis/user'
import './index.scss'


export default function ImageSharePage(props) {
    //useLocation()返回的是一个对象,包含以下信息
    //hash: ""
    //pathname: "/share"
    //search: "?urlmd5=eewhxtembaqwqwpqhsuebnvfgvjwdvjj"
    //state: undefined
    const { search } = useLocation()
    console.log('search', search)
    const [ urlmd5, setUrlMd5 ] = useState("")
    const [loading, setLoading] = useState(false)
    const [shareImageInfo, setShareImageInfo] = useState({url: '', user: '', time: '', pv: 0})
    const [showImageInfo, setShowImageInfo] = useState(false)
    
    // 获取地址栏中urlmd5参数的值
    const getUrlMd5Params = (search) => {
        // search格式 : ?urlmd5=xxxxxxxx ,所以需要从第一个字符截取
        //substring() 方法用于提取字符串中介于两个指定下标之间的字符。从下标1到结束
        const paramString = search.substring(1)
        //URLSearchParams() 方法用于解析 URL 查询字符串。固定写法，查询键值对的
        //此时searchParams是一个对象，可以通过get()方法获取urlmd5的值
        //urlmd5=eewhxtembaqwqwpqhsuebnvfgvjwdvjj
        const searchParams = new URLSearchParams(paramString)
        const urlMd5 = searchParams.get('urlmd5') || ""
        // console.log('从地址栏中获取的urlmd5值为:', urlMd5)
        setUrlMd5(urlMd5)
        return urlMd5
    }
    // 获取图片链接
    const getShareImageUrl = () => {
        const urlmd5 = getUrlMd5Params(search)
        //browseShareImage()是一个异步函数，嵌套了axios.post()方法，返回res.data
        //res.data是一个对象，包括{code: 0, urlmd5: 'afqzzxlcxdzncqgjlapopkvxfgvicetc'}
        browseShareImage({urlmd5})//.then()是一个异步函数，data参数即接受的res.data
        .then(data => {
            //跳转后的data
            //{code: 0, pv: 1, time: '2023-04-22 22:58:43', 
            //url: 'http://192.168.28.128:80/group1/M00/00/00/wKgcgGREB92ABTDwAAlGlQ2Dfx0009.jpg', 
            //user: 'seth'}
            console.log('正在走成功回调',data)
            const { code = 0 } = data
            if(code === 0) {
                const {url, user, time, pv} = data
                setShareImageInfo({url, user, time, pv})
                setShowImageInfo(true)
            }else{
                let errMsg = '文件已经被删除'
                message.error(errMsg)
            }
        })
    }
    //复制提取成功的图片url
    const copyImageUrl = () => {
        //input输入框在下边
        let transfer = document.createElement('input');
        document.body.appendChild(transfer);
        transfer.value = shareImageInfo.url  // 这里表示想要复制的内容
        transfer.focus();
        transfer.select();
        //取消聚焦
        transfer.blur();
        message.success('已复制到剪切板')
        //删除input
        document.body.removeChild(transfer);  
    }
    useEffect(() => {
        getShareImageUrl()
    }, [])

    return (
        <div className="imgInfoCon">   
            <div>
                <Card
                    hoverable
                    title="分享图片"
                    style={{ width: 800, marginTop: 20 }}
                >
                    <Descriptions column={3}>
                        <Descriptions.Item label="分享者">{shareImageInfo.user}</Descriptions.Item>
                        <Descriptions.Item label="浏览次数">{shareImageInfo.pv}</Descriptions.Item>
                        <Descriptions.Item label="分享时间">{shareImageInfo.time}</Descriptions.Item>
                        <Descriptions.Item label="图片链接">
                            { shareImageInfo.url.length > 0 ? (<div>{shareImageInfo.url} <CopyOutlined onClick={() => { copyImageUrl() }} style={{marginLeft: 10}}/></div>): null }
                        </Descriptions.Item>    
                    </Descriptions>

                    { shareImageInfo.url.length > 0 ? (<Image
                        width={300}
                        height={300}
                        src={shareImageInfo.url}
                    />) : (<Image
                        width={300}
                        height={300}
                        src="error"
                        fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                    />)}
                </Card>
            </div>
                
        </div>
             
        
    )
}