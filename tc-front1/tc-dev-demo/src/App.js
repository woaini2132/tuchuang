import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { routes, mainRoutes } from './pages';
import NavMenu from './components/NavMenu';
import { ConfigProvider, message } from 'antd';
import './App.less';
import PrivateRoute from './components/PrivateRoute';
import zhCN from 'antd/es/locale/zh_CN';
import 'moment/locale/zh-cn';
//通知框设置
message.config({
  top: 10,
  duration: 1.5,
  maxCount: 3,
  rtl: true,
});

function App() {
  return (
    //antd组件的国际化 locale={zhCN}使用中文
    <ConfigProvider locale={zhCN}>
      <div className="App">
        <Router>
          <Switch>
            {
              //map函数遍历mainroutes数组，用es6方法解析出数组中的数据（path等，将component改名c方便下边使用）
              //key值为path，path为路径，exact为精确匹配，返回一个route组件，该route组件渲染C组件
              mainRoutes.map(({ path, component: C, exact }) => {
                return <Route key={path} path={path} exact={exact}><C /></Route>
              })
            }

            <Route>
              <NavMenu />
              <Switch>
                {//同上，两个路由数组，不同的是，这里的路由是需要登录才能访问的（privateroute）
                //并且会经过div包裹，div的class为main-page
                  routes.map(({ path, component: C, exact }) => {
                    return <PrivateRoute key={path} path={path} exact={exact}><div className="main-page"><C /></div></PrivateRoute>
                  })
                }
              </Switch>
            </Route>

          </Switch>

        </Router>

      </div>
    </ConfigProvider>
  );
}

export default App;
