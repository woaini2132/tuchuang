import { getCookie } from '../utils/utils';

// 检查cookie中是否有token,用于判断用户是否登录
const checkLoginToken = () => {
    const token = getCookie('token');
    return !!token
}

export default checkLoginToken;
