
// 判断是否是对象
function isObj(param) {
    return Object.prototype.toString.call(param) === '[object Object]'
}

// 获取cookie
//cookie存储  username=123和token=123
//传入username 获取username的值
function getCookie(cname) {
    var name = cname + "=";
    //获取当前网页的所有cookie
    var decodedCookie = decodeURIComponent(document.cookie);
    //split() 方法用于把一个字符串分割成字符串数组ca。
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            //charAt() 方法可返回指定位置的字符。
            //substring() 方法用于提取字符串中介于两个指定下标之间的字符。
            //只有一个参数，即从指定位置开始到字符串结尾。
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            //indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置。
            //username是第一个字段，如果存在，返回0。
            //substring() 方法用于提取字符串中介于两个指定下标之间的字符。
            //c.substring(name.length, c.length)返回username的值。
            //name.length是username的长度。
            //c.length是第一个字段的长度。
            //返回username的值。
            return c.substring(name.length, c.length);
        }
    }//如果不存在，返回空。
    return "";
} 

// 设置cookie
function setCookie(obj) {
    // 如果传入的参数是对象 设置cookie
    if (isObj(obj)) {
        for(let key in obj) {
            //标准写法
            console.log(key, obj[key])
            document.cookie = `${key}=${obj[key]};`
        }
    }
}

// 计算文件大小
function getFileSize(size) {
    const sizeType = ['B', 'KB', "MB"]
    let finalSize = size
    //计算文件大小
    for (let sizeLevel = 0; sizeLevel < sizeType.length; sizeLevel++) {
        if (finalSize > 1024) {
            finalSize = finalSize / 1024
            continue;
        }else{
            //toFixed() 方法可把 Number 四舍五入为指定小数位数的数字。
            finalSize = finalSize.toFixed(2) + sizeType[sizeLevel]
            break;
        } 
    }
    return finalSize
}
// 根据传入类型判断文件是否是图片
function isPic(type) {

    let picSuffixStr = 'jpg,jpeg,gif,png,bmp,pic'
    let picSuffixArray = picSuffixStr.split(',')
    //includes() 方法用于判断字符串是否包含指定的子字符串。
    return picSuffixArray.includes(type)
}
//导出
module.exports = {
    getCookie,
    setCookie,
    getFileSize,
    isPic
}