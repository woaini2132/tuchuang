import axios from 'axios';
import { message } from 'antd';

export function httpGet(url, params) {
    //axios.get()返回的是一个promise对象，该对象有then方法，可以注册回调函数
    //params是一个对象。例如登录时httpPost(`${baseUrl}/login`, {user, pwd})
    return axios.get(url, { params }).then(res => {
        console.log("res打印")
        console.log(res)
        console.log(res.data)
        const { status } = res
        const { code } = res.data;
        if (status !== 200) {
            message.error('处理失败')
            throw new Error('不为成功响应200')
        }
        if (code === undefined) { // 如果处理失败
            message.error('处理失败');
            throw new Error('code不为0,处理失败');
        }
        return res.data;
    }).catch(error => {
        message.error('处理失败')
    })
}

export function httpPost(url, params) {
    return axios.post(url, params).then(res => {
        const { status } = res
        const { code = undefined } = res.data;
        console.log(res.data)
        if (status !== 200) {
            message.error('处理失败')
            throw new Error('不为成功响应200')
        }

        if (code === undefined) {// 如果处理失败
            throw new Error('返回code不为0,处理失败');
        } 

        return res.data;
    }).catch(error => {
        message.error('处理失败');
    })
}

