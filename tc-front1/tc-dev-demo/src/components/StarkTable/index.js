import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import scss from './index.module.scss';

const emptySearchData = {};
const emptyPageInfo = {};
export default function StarkTable(props) {
    const { 
        columns = [], 
        fetchData, 
        refreshTable = false, 
        searchData = emptySearchData, 
        rowKey = 'id', 
        pageInfo = emptyPageInfo,
        onTableDataLoaded=null
    } = props;

    const [data, setData] = useState({ page: 1, pageSize: 20, list: [], total: 0, ...pageInfo });
    const { page, pageSize, list, total } = data;
    const [loading, setLoading] = useState(false);
    const [innerRefresh, setInnerRefresh] = useState(false);

    const pagination = {
        total,
        pageSize,
        onChange: function (page) {
            setData({ ...data, page });
        }
    }

    useEffect(() => {
        setLoading(true);
        const { index = 5000 } = searchData;
        fetchData({ page, pageSize, index, ...searchData }).then(data => {
            setLoading(false);
            setData(data);
            // message.success('表格已刷新');
            if(onTableDataLoaded) {
                onTableDataLoaded();
            }
        });
        
    }, [page, pageSize, refreshTable, searchData, innerRefresh]);


    const onReloadClick = () => {
        setInnerRefresh(!innerRefresh);
    }

    return (
        <>
            <div className={scss.container}>
                <div>
                    <span>TOTAL: <strong>{data.total}</strong></span>
                    <span style={{ marginLeft: 10 }}>PAGE: <strong>{data.page}</strong></span>
                    <span style={{ marginLeft: 10 }}>PAGESIZE: <strong>{data.pageSize}</strong></span>
                    <span style={{ marginLeft: 10 }}>TOTALPAGE: <strong>{Math.round(data.total / data.pageSize)}</strong></span>
                    {data.totalAmount && <span style={{ marginLeft: 10 }}>TOTALAMOUNT: <strong>{data.totalAmount}</strong></span>}
                </div>

                <ReloadOutlined color="#008dff" onClick={onReloadClick} style={{marginRight: 10 }}/>
            </div>

            <Table
                rowKey={rowKey}
                dataSource={list}
                columns={columns}
                loading={loading}
                pagination={pagination}
                scroll={{ scrollToFirstRowOnChange: true }}
            />
        </>
    )
}