import React, { useState } from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import menus from './menus';
import scss from './index.module.scss';
import { getCookie } from '../../utils/utils';

const { SubMenu } = Menu;
//传入menu数组，返回一个menu组件
function renderMenu(menu) {
    const Icon = menu.icon;
    //存在子菜单
    if (menu.subMenus && menu.subMenus.length > 0) {
        const subMenus = menu.subMenus
        return (
            //subMenus是多层级的，可以嵌套子菜单。这里的key是唯一的，不能重复
            <SubMenu key={menu.key}
                title={
                    <span>
                        <Icon />
                        <span>{menu.title}</span>
                    </span>
                //递归调用renderMenu函数，传入子菜单
                }>
                {subMenus.map(subMenu => renderMenu(subMenu))}        
            </SubMenu>
        );
    } else {
        return (
            //没有子菜单，直接返回一个菜单项
            <Menu.Item key={menu.key}>
                <Link className={scss.link} to={menu.href}>
                    <Icon />
                    <span>{menu.title}</span>
                </Link>
            </Menu.Item>
        );
    }
}
//这个函数的作用是定义默认的展开的菜单和选中的菜单
function getDefaultOpenKeysAndSelectedKeys() {
    let defaultOpenKeys, defaultSelectedKeys;
    return {
        defaultOpenKeys,
        defaultSelectedKeys
    }
}

export default function NavMenu() {
    const [collapsed] = useState(false);
    const { defaultOpenKeys, defaultSelectedKeys } = getDefaultOpenKeysAndSelectedKeys();
    //从cookie获取登录名
    const username = getCookie('userName');
    ////alt属性是图片加载失败时显示的文字，src是图片的路径
    return (
    <div className={scss.navMenu}>
        <div className={scss.userInfo}>
            <img alt="logo" src="http://192.168.28.128/pub/logo192.png" />
            <div className={scss.username}>{username}</div>
        </div>
        <Menu
            //defaultSelectedKeys和defaultOpenKeys是默认选中的菜单项和展开的菜单，undefine
            //mode是菜单的模式，inline是内联模式，内敛模式下菜单项将会以行内的形式展现，同时子菜单将会被嵌套在内敛模式下的菜单项内
            //inlineCollapsed是菜单是否收起，true是收起，false是展开
            defaultSelectedKeys={defaultSelectedKeys}
            defaultOpenKeys={defaultOpenKeys}
            mode="inline"
            theme="dark"
            inlineCollapsed={collapsed}
        >
            {menus.map(menu => renderMenu(menu))}
        </Menu>
    </div>
    )
}