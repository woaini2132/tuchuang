import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { getCookie } from '../../utils/utils';
//私有路由，用于判断是否登录，如果登录了，就渲染children，否则跳转到登录页面
//这里的children是App.js中的Route组件，也就是渲染的C组件的页面
//这里的rest是App.js中的Route组件的path，exact等属性
//getcookie是utils.js中的函数，用于获取cookie
export default function PrivateRoute({ children, ...rest }) {
    const token = getCookie('token');
    const userName = getCookie('userName');
    return (
        <Route
            {...rest}//这里的rest是App.js中的Route组件的path，exact等属性,
            //这里的作用时将这些属性传递给Route组件，解引用，将属性展开不然下边的location会报错不存在
            render={({ location }) =>
                //这里的location是App.js中的Route组件的path，exact等属性
                token && userName ? (//如果都存在，就渲染子组件
                    children
                ) : (//不存在，这里重定向到登陆界面，同时将当前的location传递给login页面，用于登陆成功后跳转到当前页面
                        <Redirect
                            to={{
                                pathname: `/login`,
                                state: { from: location }
                            }}
                        />
                    )
            }
        />
    );
}