const OSS = require('ali-oss');
const fs = require('fs');
const dayjs = require('dayjs');

/**
 *console.log('\x1B[36m%s\x1B[0m', info);  //cyan
console.log('\x1B[33m%s\x1b[0m:', path);  //yellow
var styles = {
    'bold'          : ['\x1B[1m',  '\x1B[22m'],
    'italic'        : ['\x1B[3m',  '\x1B[23m'],
    'underline'     : ['\x1B[4m',  '\x1B[24m'],
    'inverse'       : ['\x1B[7m',  '\x1B[27m'],
    'strikethrough' : ['\x1B[9m',  '\x1B[29m'],
    'white'         : ['\x1B[37m', '\x1B[39m'],
    'grey'          : ['\x1B[90m', '\x1B[39m'],
    'black'         : ['\x1B[30m', '\x1B[39m'],
    'blue'          : ['\x1B[34m', '\x1B[39m'],
    'cyan'          : ['\x1B[36m', '\x1B[39m'],
    'green'         : ['\x1B[32m', '\x1B[39m'],
    'magenta'       : ['\x1B[35m', '\x1B[39m'],
    'red'           : ['\x1B[31m', '\x1B[39m'],
    'yellow'        : ['\x1B[33m', '\x1B[39m'],
    'whiteBG'       : ['\x1B[47m', '\x1B[49m'],
    'greyBG'        : ['\x1B[49;5;8m', '\x1B[49m'],
    'blackBG'       : ['\x1B[40m', '\x1B[49m'],
    'blueBG'        : ['\x1B[44m', '\x1B[49m'],
    'cyanBG'        : ['\x1B[46m', '\x1B[49m'],
    'greenBG'       : ['\x1B[42m', '\x1B[49m'],
    'magentaBG'     : ['\x1B[45m', '\x1B[49m'],
    'redBG'         : ['\x1B[41m', '\x1B[49m'],
    'yellowBG'      : ['\x1B[43m', '\x1B[49m']
};
 
 * 
 *
 */
async function uploadToOSS() {
    let client = new OSS({
        region: '',
        accessKeyId: '',
        accessKeySecret: '',
        bucket: ''
    });

    const cssDir = __dirname + '/build/static/css';
    const jsDir = __dirname + '/build/static/js';
    const cssFileList = fs.readdirSync(cssDir).map(p => cssDir + '/' + p).filter(f => /\.css$/.test(f));
    const jsFileList = fs.readdirSync(jsDir).map(p => jsDir + '/' + p).filter(f => /\.js$/.test(f));
    let uploadingFile;
    let index = 0;
    let total = cssFileList.length + jsFileList.length;
    const datetime = () => `[${dayjs().format('YY-MM-DD HH:mm:ss')}]`;
    let result;
    try {

        for (uploadingFile of cssFileList) {
            try {
                ++index;
                console.log('\x1B[37m', datetime(), `${index}/${total}`, '正在上传：', uploadingFile);
                result = await client.put(`/static/css/${uploadingFile.split('/').pop()}`, uploadingFile);
                console.log('\x1B[32m', datetime(), `${index}/${total}`, '上传成功：', result.url);
            } catch (err) {
                console.log('\x1B[31m', datetime(), `${index}/${total}`, '上传失败：', uploadingFile);
            }

        }

        for (uploadingFile of jsFileList) {
            try {
                ++index;
                console.log('\x1B[37m', datetime(), `${index}/${total}`, '正在上传：', uploadingFile);
                result = await client.put(`/static/js/${uploadingFile.split('/').pop()}`, uploadingFile);
                console.log('\x1B[32m', datetime(), `${index}/${total}`, '上传成功：', result.url);
            } catch (err) {
                console.log('\x1B[31m', datetime(), `${index}/${total}`, '上传失败：', uploadingFile);
            }
        }

        console.log('\x1B[37m', datetime(), '上传结束');

    } catch (e) {
        console.log('e: ', e);
        console.log('\x1B[31m', datetime(), `上传失败：${uploadingFile}，请稍后重试：node upload.js`);
    }
}

uploadToOSS();